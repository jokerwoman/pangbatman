export default class Ball {
    constructor(context, W, H, x, y, ballWidth, dX, dY, angleGraus, img) {
        this.x = x; // posicao inicial x
        this.y = y; // posicao inicial y
        this.angleRadianos = angleGraus * Math.PI / 180 // converter graus to radianos para ser ussada na velocidade
        this.dX = dX * Math.cos(this.angleRadianos); // Velocidade en X
        this.dY = dY * Math.sin(this.angleRadianos); // Velocidade en Y
        this.ballWidth = ballWidth; // largura e cumprimento da imagem
        this.img = new Image();
        this.img.src = img;
        this.img.height = this.ballWidth;// tamanho da bola H
        this.img.width = this.ballWidth; // tamanho da bola W
        this.W = W
        this.H = H
        this.context = context
        this.gravity = 0.1 // gravidade da bola é aplicada no dY
    }

    draw() {
        this.context.drawImage(this.img, this.x, this.y, this.ballWidth, this.ballWidth);
    }

    update() {
        // colisões verticais
        if (this.x < 0 || this.x > this.W - this.ballWidth)
            this.dX = -this.dX; // trocar a direção ao contrario da bola nas paredes verticais 

        // colisões horizontais
        if (this.y >=  this.H - this.ballWidth)
        {
            //Quando a bola esta quase a tocar o chão troca de sinal
            this.dY = -this.dY;
        }
        else{
             //Quando a bola não toca o chão
            this.dY += this.gravity
        }

        this.x += this.dX;
        this.y += this.dY;
    }

}