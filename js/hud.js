
export default class Hud {
    constructor(ctx, W, H, x, y, player1Life, player2Life , imgPlayer1FullLife, imgPlayer1MediumLife, imgPlayer1LowLife,imgPlayer2FullLife, imgPlayer2MediumLife, imgPlayer2LowLife, lifeWidth, lifeHeigth) {
        this.x = x; // posicao inicial x
        this.y = y; // posicao inicial y
        this.W = W
        this.H = H
        this.ctx = ctx
        this.totalPoints = 0
        
        this.imgPlayer1FullLife = new Image()
        this.imgPlayer1FullLife.src = imgPlayer1FullLife
        this.imgPlayer1FullLife.width = lifeWidth
        this.imgPlayer1FullLife.height = lifeHeigth

        this.imgPlayer2FullLife = new Image()
        this.imgPlayer2FullLife.src = imgPlayer2FullLife
        this.imgPlayer2FullLife.width = lifeWidth
        this.imgPlayer2FullLife.height = lifeHeigth
        
        this.imgPlayer1MediumLife = new Image()
        this.imgPlayer1MediumLife.src = imgPlayer1MediumLife
        this.imgPlayer1MediumLife.width = lifeWidth
        this.imgPlayer1MediumLife.height = lifeHeigth
        
        this.imgPlayer2MediumLife = new Image()
        this.imgPlayer2MediumLife.src = imgPlayer2MediumLife
        this.imgPlayer2MediumLife.width = lifeWidth
        this.imgPlayer2MediumLife.height = lifeHeigth

        this.imgPlayer1LowLife = new Image()
        this.imgPlayer1LowLife.src = imgPlayer1LowLife
        this.imgPlayer1LowLife.width = lifeWidth
        this.imgPlayer1LowLife.height = lifeHeigth

        this.imgPlayer2LowLife = new Image()
        this.imgPlayer2LowLife.src = imgPlayer2LowLife
        this.imgPlayer2LowLife.width = lifeWidth
        this.imgPlayer2LowLife.height = lifeHeigth

        this.player1Lifex = 35 //posiçaõ em x
        this.player1Lifey = 3 //posiçaõ em y

        this.player1Life = player1Life // vida

        this.player2Lifex = 200 //posiçaõ em x
        this.player2Lifey = 3 //posiçaõ em y

        this.player2Life = player2Life // vida
    }
    
    drawPoints()
    {
        this.ctx.font = "30px bold Courier New";
        this.ctx.fillText("Points: " + this.totalPoints.toString(),this.x,this.y);
    }

    //retorna a vida de cada jogador
    getPlayerLife(playerIndex)
    {
        if(playerIndex == 1)
        {
            return this.player1Life
        }
        else{
            return this.player2Life
        }
    }

    drawLife(playerIndex, playerLife, playerLifex, playerLifey)
    {
        switch(playerLife)
        {
            default:
            case 0:
            {
                // Não há vida, não desenho
                break;
            }
            case 3:
            {
                if(playerIndex == 1)
                {
                    this.ctx.drawImage(this.imgPlayer1FullLife, playerLifex, playerLifey, this.imgPlayer1FullLife.width, this.imgPlayer1FullLife.height);
                }
                else{
                    this.ctx.drawImage(this.imgPlayer2FullLife, playerLifex, playerLifey, this.imgPlayer2FullLife.width, this.imgPlayer2FullLife.height);
                }
                break;
            }    
            case 2:
            {
                if(playerIndex == 1)
                {
                    this.ctx.drawImage(this.imgPlayer1MediumLife, playerLifex, playerLifey, this.imgPlayer1MediumLife.width, this.imgPlayer1MediumLife.height);
                }
                else{
                    this.ctx.drawImage(this.imgPlayer2MediumLife, playerLifex, playerLifey, this.imgPlayer2MediumLife.width, this.imgPlayer2MediumLife.height);
                }
                break;
            }
            case 1:
            {
                if(playerIndex == 1)
                {
                    this.ctx.drawImage(this.imgPlayer1LowLife, playerLifex, playerLifey, this.imgPlayer1LowLife.width, this.imgPlayer1LowLife.height);
                }
                else{
                    this.ctx.drawImage(this.imgPlayer2LowLife, playerLifex, playerLifey, this.imgPlayer2LowLife.width, this.imgPlayer2LowLife.height);
                }
                break;
            }
        }
    }
    //quando o jogador vai perdendo vida
    playerLostLife(playerIndex)
    {
        // quando houver uma colisão com uma bola o jogador perde uma vida
        if(playerIndex == 1)
        {
            if(this.player1Life > 0)
            {
                this.player1Life -= 1
            }
        }
        else{
            if(this.player2Life > 0)
            {
                this.player2Life -= 1
            }
        }

    }

    draw() {
        this.drawPoints()
        this.drawLife(1, this.player1Life, this.player1Lifex, this.player1Lifey)
        this.drawLife(2, this.player2Life, this.player2Lifex, this.player2Lifey)
    }

    update(points) {
        // atualizar pontos
        this.totalPoints += points
    }

}