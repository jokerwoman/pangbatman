
import Rectangulo from './colisao.js'

export default class MenuStart {
    constructor(ctx, W, H, imgSelected, imgNotSelected) {
        this.buttonsWidth = 250
        this.buttonsHeight = 100

        this.imgSelected = new Image()
        this.imgSelected.src = imgSelected
        this.imgSelected.width = this.buttonsWidth
        this.imgSelected.height = this.buttonsHeight

        this.imgNotSelected = new Image()
        this.imgNotSelected.src = imgNotSelected
        this.imgNotSelected.width = this.buttonsWidth
        this.imgNotSelected.height = this.buttonsHeight

        // Single Player
        this.button1x = W/8
        this.button1y = H/4

        // Multi Player
        this.button2x = W/8
        this.button2y = H/2

        this.W = W
        this.H = H
        this.ctx = ctx

        this.buttonSelected = -1 // inicializamos com nehum botao selecionado
    }

    draw() {

        switch(this.buttonSelected)
        {
            default:
            case -1:
            {
                //nemhum botão seleccionado (enganeime e fiz ao contrario)
                this.ctx.drawImage(this.imgSelected, this.button1x, this.button1y, this.buttonsWidth, this.buttonsHeight)
                this.ctx.drawImage(this.imgSelected, this.button2x, this.button2y, this.buttonsWidth, this.buttonsHeight)
                break;
            }
            case 1:
            {
                // primer botão seleccionado
                this.ctx.drawImage(this.imgNotSelected, this.button1x, this.button1y, this.buttonsWidth, this.buttonsHeight)
                this.ctx.drawImage(this.imgSelected,this.button2x, this.button2y, this.buttonsWidth, this.buttonsHeight)
                break
            }
            case 2:
            {
                // segundo botão seleccionado
                this.ctx.drawImage(this.imgSelected, this.button1x, this.button1y, this.buttonsWidth, this.buttonsHeight)
                this.ctx.drawImage(this.imgNotSelected, this.button2x, this.button2y, this.buttonsWidth, this.buttonsHeight)
                break;
            }
        }

        /* Escrever os textos dos botoes*/
        this.ctx.font = "30px bold Courier New";
        this.ctx.fillStyle = "white";
        this.ctx.fillText("Single", this.button1x + 85, this.button1y + 59 );
        this.ctx.fillText("Multiplayer", this.button2x + 52, this.button2y + 59 );
    }

    getButtonSelected()
    {
        return this.buttonSelected  // funçao ha ser ussada no game para saber qual dos botoes esta select
    }

    update(posicaoRatoX, posicaoRatoY)
    {
        // Verificar a colisão do rectangulo do botão 1 e 2 com o mouse posicion Rectangulo(W e H a 0) 
        let rectA = new Rectangulo(this.button1x, this.button1y,this.imgSelected.width, this.imgSelected.height) // butao 1
        let rectB = new Rectangulo(this.button2x, this.button2y,this.imgSelected.width, this.imgSelected.height) // butao 2
        let rectC = new Rectangulo(posicaoRatoX, posicaoRatoY, 0,0 )

        let menuOneSelecionado = rectA.checkColisaoDeRetangulos(rectC)
        let menuTwoSelecionado = rectB.checkColisaoDeRetangulos(rectC)

        
        if(menuOneSelecionado == true)
        {
            this.buttonSelected = 1 // butao 1 selecionado
        }
        else if(menuTwoSelecionado == true)
        {
            this.buttonSelected = 2 // butao 2 selecionado
        }
        else{
            this.buttonSelected = -1 // nenhum butao selecionado
        }
    }

}