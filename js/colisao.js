export default class Rectangulo{
    constructor(x, y , W, H) {
        // esta classe rectangulo serve para ser ussada para as colisões estre rectangulos
        this.x = x 
        this.y = y
        this.W = W
        this.H = H
    }

    checkColisaoDeRetangulos(rectB)
    {
        /*
        * esta função é ussada para saber se existio colisão entre o rectangulo player e uma bola e tambem se houve colisão entre harpoon e bola.
        * esta função tambem é ussada para as colisões do rectangulo onde o rato esta (com W= 0 e H=0) com os retangulo de seleção 1 player ou 2 players 
        */
        let estadoColisao

        if((this.x + this.W < rectB.x) || // this rectangulo esta a esquerda da rectangulo B
           (this.x > rectB.x + rectB.W) || // this rectangulo esta a direita da rectangulo B
           (this.y + this.H < rectB.y) || // this rectangulo esta a acima da rectangulo B
           (this.y > rectB.y + rectB.H)) // this rectangulo esta abaixo da rectangulo B
        {
            estadoColisao = false
        }
        else
        {
            // Colisão aconteceu
            estadoColisao = true
        }
        return estadoColisao
    }
}