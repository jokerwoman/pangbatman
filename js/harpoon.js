

import Rectangulo from './colisao.js'

export default class Harpoon {
    constructor(ctx, W, H, x, imgLine, imgHarpoon) {
        this.imgHarpoon = new Image()
        this.imgHarpoon.src = imgHarpoon
        this.imgHarpoon.height = 27
        this.imgHarpoon.width = 57

        this.imgLine = new Image()
        this.imgLine.src = imgLine
        this.lineWidth = 4


        this.x = x // posicao inicial x
        this.y = H - this.imgHarpoon.height // posicao inicial y
        this.dY = 8 // velocidade do harpoon
        this.W = W
        this.H = H
        this.ctx = ctx
        this.harpoonTeto = false // variavel para saber se o harpoon chegou ao teto
    }

    harpoonTocaTeto(){
        return this.harpoonTeto
    }

    draw() {
        //disenhar linha do harpoon com pattern de imagem
        this.ctx.beginPath();
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.shadowBlur = 10;
        this.ctx.setLineDash([15, 3, 3, 3]); // padrão de dashes
        this.ctx.shadowColor = "#0C0301"
        this.ctx.strokeStyle = this.ctx.createPattern(this.imgLine, 'repeat') // cria um patrao repitendo a imagem
        this.ctx.moveTo(this.x + (this.imgHarpoon.width / 2), this.H)
        this.ctx.lineTo(this.x + (this.imgHarpoon.width / 2), this.y + this.imgHarpoon.height)
        this.ctx.stroke();

        // Disenhar parte cima do harpoon 
        this.ctx.shadowColor = "transparent"
        this.ctx.drawImage(this.imgHarpoon, this.x, this.y, this.imgHarpoon.width, this.imgHarpoon.height);
    }

    checkBallColision(ball)  
    {
        //verificação da colisão da bola com a parte cima do harpoon e tambem com a linha do harpoon
        let rectA = new Rectangulo(this.x, this.y, this.imgHarpoon.width, this.imgHarpoon.height)
        let rectB = new Rectangulo(ball.x, ball.y, ball.img.width, ball.img.height)
        let colisaoHarpoon = rectA.checkColisaoDeRetangulos(rectB)

        rectA = new Rectangulo(this.x + (this.imgHarpoon.width / 2), this.y + this.imgHarpoon.height, this.lineWidth, this.H - this.y - this.imgHarpoon.height)
        rectB =  new Rectangulo(ball.x, ball.y, ball.img.width, ball.img.height)
        let colisaoLine = rectA.checkColisaoDeRetangulos(rectB)

        return (colisaoHarpoon || colisaoLine)
    }

    update() {
        // colisões horizontal
        if (this.y > 0) {
            //Se ainda não chegou ao teto continua a subir
            this.y -= this.dY;
        }else{
            //ja chegou ao teto não fazemos mais update do y
            this.harpoonTeto = true
        }
    }

}