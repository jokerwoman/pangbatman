import Player from './player.js';
import Ball from './ball.js';
import Background from './background.js';
import Harpoon from './harpoon.js';
import Hud from './hud.js';
import MenuStart from './menuStart.js'
import { GetCanvasHTMLElements } from './htmlElements.js';
import { GetLoadingPageHTMLElements } from './htmlElements.js';
import { GetPresentationPageHTMLElements } from './htmlElements.js';

// tem o estado dos botoes do jogo
var navegacao = { arrowUp: false, arrowLeft: false, arrowRigth: false, keyA: false, keyW: false, keyD: false }; 

const direcoes = {
    PARADO: 0,
    IZQUERDA: 1,
    DIREITA: 2,
    DISPARO: 3
}

const bolasTamanhos = {
    GRANDE: 80,
    MEDIO: 40,
    PEQUENO: 20
}

const bolasAlturas = { // bolas tem o mesmo tamanho
    GRANDE: 150
}

const bolasPontos = {
    GRANDE: 300,
    MEDIO: 250,
    PEQUENO: 200
}

const levels = {
    PRESENTATION_PAGE: 0,
    LOADING_PAGE: 1,
    START_SCREEN: 2,
    LEVEL_UM: 3,
    LEVEL_DOIS: 4,
    LEVEL_TRES: 5,
    GAME_OVER: 6,
    GAME_WIN: 7
}

const players = {
    SINGLE_PLAYER: 0,
    MULTI_PLAYER: 1
}

var canvas = null
var context = null
var currentLevel = levels.PRESENTATION_PAGE // esta variavel serve ao longo do jogo para saber em que level estamos
var gameRunning = false
var gamePlayers = players.SINGLE_PLAYER
var hud = null
var batman = null
var robin = null
var balls = null
var harpoonBatman = null
var harpoonRobin = null
var background = null
let menuStart = null

let posicaoRatoX = -1
let posicaoRatoY = -1
let posicaoRatoClicked = -1

function CreateLevelUm()
{
    if(gameRunning === false)   // se o jogo passou para o level 1 (new jogo) criamos um novo HUD.
    { 
        // Se não é multiplayer o Robin tem 0 vidas
        let robinLifes = (gamePlayers === players.MULTI_PLAYER) ? 3 : -1
        
        // Se o jogo començou pela primeira vez criamos um HUD
        gameRunning = true
        hud = new Hud(context, canvas.width, canvas.height, 625, 30, 3, robinLifes ,'img/vida/fullBatman.png', 'img/vida/mediumBatman.png', 'img/vida/lowBatman.png',
                                                                                   'img/vida/fullRobin.png', 'img/vida/mediumRobin.png', 'img/vida/lowRobin.png',
        127, 44)
    }

    background = new Background(context, canvas.width, canvas.height, 'img/background/nivelUm.png')

    if(hud.getPlayerLife(1) > 1)
    {
        batman = new Player(context, canvas.width, 504 / 9, 56, 200, 309, 9, 2, 4, 35,
            'img/batman/esquerda.png', 'img/batman/direita.png', 'img/batman/parado.png', 'img/batman/disparar.png')
    }

    if((gamePlayers === players.MULTI_PLAYER) && (hud.getPlayerLife(2) > 1))
    {
        robin = new Player(context, canvas.width, 448 / 8, 56, 600, 309, 8, 2, 4, 35,
            'img/robin/esquerda.png', 'img/robin/direita.png', 'img/robin/parado.png', 'img/robin/disparar.png')
    }


    balls = new Array()
    balls.push(new Ball(context, canvas.width, canvas.height, 210, bolasAlturas.GRANDE, bolasTamanhos.GRANDE, 30, -4, 85, 'img/ball/ball.png'));
}

function CreateLevelDois()
{
    currentLevel = levels.LEVEL_DOIS

    background = new Background(context, canvas.width, canvas.height, 'img/background/nivelDois.png')

    if(hud.getPlayerLife(1) > 1)
    {
        batman = new Player(context, canvas.width, 504 / 9, 56, 200, 309, 9, 2, 4, 35,
            'img/batman/esquerda.png', 'img/batman/direita.png', 'img/batman/parado.png', 'img/batman/disparar.png')
    }

    if((gamePlayers === players.MULTI_PLAYER) && (hud.getPlayerLife(2) > 1))
    {
        robin = new Player(context, canvas.width, 448 / 8, 56, 600, 309, 8, 2, 4, 35,
            'img/robin/esquerda.png', 'img/robin/direita.png', 'img/robin/parado.png', 'img/robin/disparar.png')
    }

    balls = new Array()
    balls.push(new Ball(context, canvas.width, canvas.height, 540, bolasAlturas.GRANDE, bolasTamanhos.PEQUENO, -30, -2, 85, 'img/ball/ball.png'));
    balls.push(new Ball(context, canvas.width, canvas.height, 570, bolasAlturas.GRANDE, bolasTamanhos.PEQUENO, 30, -2, 85, 'img/ball/ball.png'));
    
    balls.push(new Ball(context, canvas.width, canvas.height, 310, bolasAlturas.GRANDE, bolasTamanhos.MEDIO, -30, -2, 85, 'img/ball/ball.png'));
    balls.push(new Ball(context, canvas.width, canvas.height, 280, bolasAlturas.GRANDE, bolasTamanhos.MEDIO, 30, -2, 85, 'img/ball/ball.png'));
}

function CreateLevelTres()
{
    currentLevel = levels.LEVEL_TRES

    background = new Background(context, canvas.width, canvas.height, 'img/background/nivelTres.jpg')

    if(hud.getPlayerLife(1) > 1)
    {
        batman = new Player(context, canvas.width, 504 / 9, 56, 200, 309, 9, 2, 4, 35,
            'img/batman/esquerda.png', 'img/batman/direita.png', 'img/batman/parado.png', 'img/batman/disparar.png')
    }

    if((gamePlayers === players.MULTI_PLAYER) && (hud.getPlayerLife(2) > 1))
    {
        robin = new Player(context, canvas.width, 448 / 8, 56, 600, 309, 8, 2, 4, 35,
            'img/robin/esquerda.png', 'img/robin/direita.png', 'img/robin/parado.png', 'img/robin/disparar.png')
    }

    balls = new Array()
    balls.push(new Ball(context, canvas.width, canvas.height, 210, bolasAlturas.GRANDE, bolasTamanhos.GRANDE, 30, -4, 85, 'img/ball/ball.png'));
    balls.push(new Ball(context, canvas.width, canvas.height, 450, bolasAlturas.GRANDE, bolasTamanhos.GRANDE, -30, 4, 85, 'img/ball/ball.png'));
}

function MenuSelectPlayer()
{
    //MENU SELEÇÃO DOS JOGADORES 
    let gameStart = false
    context.clearRect(0, 0, canvas.width, canvas.height); // limpa o canvas

    background.draw()
    menuStart.draw()
    menuStart.update(posicaoRatoX, posicaoRatoY) // calcular se o rato esta encima dos single ou multiplayer

    let menuSelected = menuStart.getButtonSelected()

    if(menuSelected !== -1) // esta selecionado uma opcao
    {
        if(posicaoRatoClicked === true)
        {
            if(menuSelected == 1)
            {
                // single player
                gamePlayers = players.SINGLE_PLAYER
            }else{
                // multiplayer
                gamePlayers = players.MULTI_PLAYER
            }
            gameStart = true // e comença o jogo
        }
    }

    if(gameStart === false)
    {
        requestAnimationFrame(MenuSelectPlayer)
    }
    else
    {
        // Jogo comença no level 1 e ja foi escolhido a opção 
        currentLevel = levels.LEVEL_UM
        CreateLevels()
        gameLoop()
    }

}

function GameWinDraw()
{
    context.clearRect(0, 0, canvas.width, canvas.height); // limpa o canvas
    background.draw()
    hud.draw()

    if(currentLevel == levels.GAME_WIN)
    {
        requestAnimationFrame(GameWinDraw)
    }
}

function GameWin()
{
    robin = null
    batman = null
    gameRunning = false;
    currentLevel = levels.GAME_WIN
    background = new Background(context, canvas.width, canvas.height, 'img/background/gameWin.png')
    setTimeout(() => { StartMenu()}, 5000);
    requestAnimationFrame(GameWinDraw)
}

function GameOverDraw()
{
    context.clearRect(0, 0, canvas.width, canvas.height); // limpa o canvas
    background.draw()
    hud.draw()

    if(currentLevel == levels.GAME_OVER)
    {
        requestAnimationFrame(GameOverDraw)
    }
}

function GameOver()
{    
    robin = null
    batman = null
    gameRunning = false;
    currentLevel = levels.GAME_OVER
    background = new Background(context, canvas.width, canvas.height, 'img/background/gameOver.png')
    setTimeout(() => { StartMenu()}, 5000);
    requestAnimationFrame(GameOverDraw)
}

function StartMenu()
{    
    // Começa o menu de seleção de quantos jogadores single ou multi
    currentLevel = levels.START_SCREEN
    menuStart = new MenuStart(context, canvas.width, canvas.height, 'img/button/notSelected.png', 'img/button/selected.png')
    background = new Background(context, canvas.width, canvas.height, 'img/background/menuStart.png')
    MenuSelectPlayer()
}

function IniciarJogo()
{
    // Chamamos esta função quando clickamos no space e estamos no loading page
    document.body.innerHTML = GetCanvasHTMLElements();

    canvas = document.getElementById('myCanvas')
    context = canvas.getContext('2d')

    
    canvas.addEventListener('mousemove', e =>{
        posicaoRatoX = e.offsetX
        posicaoRatoY = e.offsetY
    })

    canvas.addEventListener('mousedown', e =>{
        posicaoRatoClicked = true
    })

    canvas.addEventListener('mouseup', e =>{
        posicaoRatoClicked = false
    })

    StartMenu()
}

function StartPresentationPage()
{
    setTimeout(() => { StartLoadingPage()}, 10000);
    document.body.innerHTML = GetPresentationPageHTMLElements();
}

function StartLoadingPage()
{
    document.body.style.backgroundImage = "url(img/background/backgroundCover.jpg)"; // mudar de preto para a imagem do joker
    currentLevel = levels.LOADING_PAGE
    document.body.innerHTML = GetLoadingPageHTMLElements();
}

function DirecaoMovimentoBatman() {
    let direcao = direcoes.PARADO

    if (navegacao.arrowUp == true) {
        direcao = direcoes.DISPARO
    }
    else if (navegacao.arrowLeft == true) {
        direcao = direcoes.IZQUERDA
    } else if (navegacao.arrowRigth == true) {
        direcao = direcoes.DIREITA
    }

    return direcao
}

function DirecaoMovimentoRobin() {
    let direcao = direcoes.PARADO

    if (navegacao.keyW == true) {
        direcao = direcoes.DISPARO
    }
    else if (navegacao.keyA == true) {
        direcao = direcoes.IZQUERDA
    } else if (navegacao.keyD == true) {
        direcao = direcoes.DIREITA
    }

    return direcao
}

function CheckBatmanHarpoonExists() {
    return (harpoonBatman !== null)
}

function CheckRobinHarpoonExists() {
    return (harpoonRobin !== null)
}

function CreateBatmanHarpoon() {
    return new Harpoon(context, canvas.width, canvas.height, batman.getPosicaoX(), 'img/harpoonBatman/metal.png', 'img/harpoonBatman/harpoon.png');
}
function CreateRobinHarpoon() {
    return new Harpoon(context, canvas.width, canvas.height, robin.getPosicaoX(), 'img/harpoonRobin/metal.png', 'img/harpoonRobin/harpoon.png');
}

function gameLoop() {
    // esta função é o coração do jogo
    context.clearRect(0, 0, canvas.width, canvas.height); // limpa o canvas

    background.draw()
    hud.draw()

    if(batman !== null){
        if (DirecaoMovimentoBatman() == direcoes.DISPARO && (CheckBatmanHarpoonExists() == false)) {
            harpoonBatman = CreateBatmanHarpoon() // se clickamos pra cima criamos um harpoon para batman (se nao exise harpoon)
        }
    }   

    if (CheckBatmanHarpoonExists() == true) {
        if (harpoonBatman.harpoonTocaTeto() === true) {
            harpoonBatman = null
        } else {
            harpoonBatman.draw()
            harpoonBatman.update()
        }
    }
    if(robin !== null)
    {
        if (DirecaoMovimentoRobin() == direcoes.DISPARO && (CheckRobinHarpoonExists() == false)) {
            harpoonRobin = CreateRobinHarpoon()
        }
    }

    if (CheckRobinHarpoonExists() == true) {
        if (harpoonRobin.harpoonTocaTeto() === true) {
            harpoonRobin = null
        } else {
            harpoonRobin.draw()
            harpoonRobin.update()
        }
    }
    
    if(balls.length == 0)
    {
        // Se não á nehuma bola o level acabou
        if(currentLevel === levels.LEVEL_UM)
        {
            currentLevel = levels.LEVEL_DOIS
            CreateLevelDois()
            requestAnimationFrame(gameLoop);
        }
        else if(currentLevel === levels.LEVEL_DOIS){
            currentLevel = levels.LEVEL_TRES
            CreateLevelTres()
            requestAnimationFrame(gameLoop);
        }
        else if(currentLevel === levels.LEVEL_TRES){
            GameWin()
        }
    }
    else{
        balls.some(function (ball, index) {
        // Fazer um loop usando o some da todas as bolas e caso exista colisão do batman ou robin interrumper o loop inmediatamente
        // para interrumper inmediatamente retorna true caso contrario retorna falso
        // nao usei o for each para poder interrumper mal aconteça uma colisão se não  não conseguia parar o loop
        let colisao = false
        let pontos = 0

        if(batman !== null){

            if (true === batman.checkPlayerColision(ball)) {
                hud.playerLostLife(1)                
                let batmanLifes = hud.getPlayerLife(1)
                if(batmanLifes < 1)
                {
                    batman = null
                    if(gamePlayers === players.SINGLE_PLAYER)
                    {
                        gameRunning = false
                    }
                    else if(robin === null && (gamePlayers === players.MULTI_PLAYER))
                    {
                        gameRunning = false
                    }
                    return true // forcar a paragem do loop do array de bolas mal um jogador seja atingido
                }

                if(gameRunning === true)
                {
                   CreateLevels()
                }
            }
        }

        if(robin !== null){

            if (true === robin.checkPlayerColision(ball)) {
                hud.playerLostLife(2)
                let robinLifes = hud.getPlayerLife(2)
                if(robinLifes < 1)
                {
                    robin = null
                    if(batman === null && (gamePlayers === players.MULTI_PLAYER))
                    {
                        gameRunning = false
                    }
                    return true // forcar a paragem do loop do array de bolas mal um jogador seja atingido
                }

                if(gameRunning === true)
                {
                    CreateLevels()
                }
            }
        }

        if (CheckBatmanHarpoonExists() == true) {
            if (true === harpoonBatman.checkBallColision(ball)) {
                colisao = true
                harpoonBatman = null
            }
        }

        if (CheckRobinHarpoonExists() == true) {
            if (true === harpoonRobin.checkBallColision(ball)) {
                if (colisao === false) {
                    //se o harpoon de batman nao tocou primeiro o robin foi quem acertou na bola
                    harpoonRobin = null
                    colisao = true
                }
            }
        }

        if (colisao === true) {
            // Quando existe uma colisão as bolas dividense 
            if (ball.ballWidth === bolasTamanhos.GRANDE) {
                pontos += bolasPontos.GRANDE                                       //ball y
                balls.push(new Ball(context, canvas.width, canvas.height, ball.x, bolasAlturas.GRANDE, bolasTamanhos.MEDIO, -30, -2.5, 85, 'img/ball/ball.png'));
                balls.push(new Ball(context, canvas.width, canvas.height, ball.x, bolasAlturas.GRANDE, bolasTamanhos.MEDIO, 30, -2.5, 85, 'img/ball/ball.png'));
            }
            else if (ball.ballWidth === bolasTamanhos.MEDIO) {
                pontos += bolasPontos.MEDIO
                balls.push(new Ball(context, canvas.width, canvas.height, ball.x, bolasAlturas.GRANDE, bolasTamanhos.PEQUENO, -30, -2, 85, 'img/ball/ball.png'));
                balls.push(new Ball(context, canvas.width, canvas.height, ball.x, bolasAlturas.GRANDE, bolasTamanhos.PEQUENO, 30, -2, 85, 'img/ball/ball.png'));
            }
            else { // bolasTamanhos.Pequeno
                pontos += bolasPontos.PEQUENO
            }
            hud.update(pontos)
            balls.splice(index, 1) // remover bola colidida 
        }

        ball.draw()
        ball.update()
        return false // continuar o loop das bolas para verificar mais colisões
    });
    

        if(batman !== null){
            batman.draw()
            batman.update(DirecaoMovimentoBatman())
        }
        
        if(robin !== null)
        {
            robin.draw()
            robin.update(DirecaoMovimentoRobin())
        }

        if(gameRunning === false)
        {
            GameOver()
        }
        else{
            requestAnimationFrame(gameLoop);
        }
    }
}

function CreateLevels()
{
    if(currentLevel === levels.LEVEL_UM)
    {
        CreateLevelUm()
    }
    else if(currentLevel === levels.LEVEL_DOIS)
    {
        CreateLevelDois()
    }
    else if(currentLevel === levels.LEVEL_TRES)
    {
        CreateLevelTres()
    }
}

document.addEventListener('keydown', function (event) {

    switch (event.code) {
        case "KeyS":
        case "ArrowDown":

            break;
        case "KeyW":
            navegacao.keyW = true
            break;
        case "ArrowUp":
            navegacao.arrowUp = true

            break;
        case "KeyA":
            navegacao.keyA = true
            break;
        case "ArrowLeft":
            {
                navegacao.arrowLeft = true
            }
            break;
        case "KeyD":
            navegacao.keyD = true
            break;
        case "ArrowRight":
            {
                navegacao.arrowRigth = true
            }
            break;
    }
});

document.addEventListener('keyup', function (event) {

    switch (event.code) {
        case "KeyS":
        case "ArrowDown":

            break;
        case "KeyW":
            navegacao.keyW = false
            break;
        case "ArrowUp":
            navegacao.arrowUp = false

            break;
        case "KeyA":
            navegacao.keyA = false
            break;
        case "ArrowLeft":
            {
                navegacao.arrowLeft = false
            }
            break;
        case "KeyD":
            navegacao.keyD = false
            break;
        case "ArrowRight":
            {
                navegacao.arrowRigth = false
            }
            break;
            case "Space":
            {
                if(currentLevel === levels.LOADING_PAGE)
                {
                    /* se o utilizador click no space e estiver no loading page inicia o jogo onde carrega o canvas */
                    IniciarJogo()
                }
                break;
            }
    }
});


/* Fazer load do presentation Page */
StartPresentationPage();