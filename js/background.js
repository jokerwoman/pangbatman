
export default class Background {
    constructor(context, width, height, img) {
        this.img = new Image();
        this.img.src = img;
        this.width = width
        this.height = height
        this.context = context
    }

    draw() {
        //desenhamos o Background
        this.context.drawImage(this.img, 0, 0, this.width, this.height);
    }
}
