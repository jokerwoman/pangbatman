import Rectangulo from './colisao.js'

const direcoes = {
    PARADO: 0,
    IZQUERDA: 1,
    DIREITA: 2,
    DISPARO: 3
}

export default class Player {
    constructor(context, width, widthPlayerCharacter, heightPlayerCharacter, posicaoX, posicaoY,
        nroImagensDerEsq, nroImagensParado, nroImagensDisparo, personagemResize,
        imagemEsquerda, imagemDireita, imagemParado, imagemDisparo) {       //construtor    
        this.currentPlayerFrameEsquerdaDireita = 0; // frame atual que pode ir ate x elementos - esquerda e direita
        this.currentPlayerFrameParado = 0; // frame atual que pode ir ate x elementos - parado
        this.currentPlayerFrameDisparo = 0; // frame atual que pode ir ate x elementos - parado loop sprite
        this.widthPlayerCharacter = widthPlayerCharacter // largura do personagem 
        this.heightPlayerCharacter = heightPlayerCharacter // altura do personagem
        this.x = posicaoX // meio do tabuleiro = 800 (canvas.width ) / 2 - 56 (widthPlayerCharacter) / 2
        this.y = posicaoY // altura 0 = 400 (canvas.heigth ) - 56 (heightPlayerCharacter) - personagemResize
        this.dX = 5 // 5 pixels por frame (velocidade)
        this.direcaoPlayer = direcoes.PARADO
        this.nroImagensDerEsq = nroImagensDerEsq
        this.nroImagensParado = nroImagensParado
        this.nroImagensDisparo = nroImagensDisparo
        this.personagemResize = personagemResize
        this.imagemEsquerda = new Image()
        this.imagemEsquerda.src = imagemEsquerda;
        this.imagemDireita = new Image()
        this.imagemDireita.src = imagemDireita;
        this.imagemParado = new Image()
        this.imagemParado.src = imagemParado;
        this.imagemDisparo = new Image()
        this.imagemDisparo.src = imagemDisparo;
        this.updateSpriteFrameRate = 10 // a cada 10 frames disenhados fazemos update da proxima imagen dentro da imagem a ser mostrada
        this.frameCounter = 0 // quando esta variavel fora igual a 10 pasamos para a proxima imagem dentro do esprite
        this.width = width
        this.offset = 20 // batman e robin tem muitos pixels transparentes, reduzir o tamanho do rectangulo
        this.context = context
    }

    getPosicaoX() {
        return this.x // cuando o player dispara um harpoon o harpoon tem que començar na posição onde o player estava
    }

    update(direcao) {
        /* Update da posicao do player consoante e esquerda ou direita o movimento */
        if (this.direcaoPlayer == direcoes.IZQUERDA) {
            if (this.x - this.dX > 0) {
                this.x -= this.dX;
            }
        } else if (this.direcaoPlayer == direcoes.DIREITA) {
            if (this.x + this.dX + this.widthPlayerCharacter + this.personagemResize < this.width) {
                this.x += this.dX;
            }
        }

        /* Actualizar elemento do sprite consoante o numero de frames que ja foram mostrados */
        this.frameCounter++

        /* Sempre que a direcao do player mudar posicionar o sprit para a primeira posicao de imagem */
        if (this.direcaoPlayer !== direcao) {
            this.direcaoPlayer = direcao
            this.currentPlayerFrameEsquerdaDireita = 0
            this.currentPlayerFrameParado = 0;
            this.currentPlayerFrameDisparo = 0;
        }

        /* A cada 10 frames mudar para a proxima imagem do sprite */
        if (this.frameCounter == this.updateSpriteFrameRate) {
            this.frameCounter = 0
            if ((this.direcaoPlayer == direcoes.IZQUERDA) || (this.direcaoPlayer == direcoes.DIREITA)) {
                this.currentPlayerFrameEsquerdaDireita += 1;
                this.currentPlayerFrameEsquerdaDireita = this.currentPlayerFrameEsquerdaDireita % this.nroImagensDerEsq;
            }
            else if (this.direcaoPlayer == direcoes.PARADO) {
                this.currentPlayerFrameParado += 1;
                this.currentPlayerFrameParado = this.currentPlayerFrameParado % this.nroImagensParado;
            }
            else if (this.direcaoPlayer == direcoes.DISPARO) {
                this.currentPlayerFrameDisparo += 1;
                this.currentPlayerFrameDisparo = this.currentPlayerFrameDisparo % this.nroImagensDisparo;
            }
        }
    }

    checkPlayerColision(ball)  
    {
        let rectA = new Rectangulo(this.x + this.offset, this.y + this.offset, this.widthPlayerCharacter - this.offset , this.heightPlayerCharacter - this.offset)
        let rectB = new Rectangulo(ball.x, ball.y, ball.img.width, ball.img.height)
        let colisaoPlayer = rectA.checkColisaoDeRetangulos(rectB)

        return colisaoPlayer
    }

    draw() {
        if (this.direcaoPlayer == direcoes.IZQUERDA) {
            this.context.drawImage(this.imagemEsquerda, this.currentPlayerFrameEsquerdaDireita * this.widthPlayerCharacter, 0, this.widthPlayerCharacter, this.heightPlayerCharacter, this.x, this.y, this.widthPlayerCharacter + this.personagemResize, this.heightPlayerCharacter + this.personagemResize);
        } else if (this.direcaoPlayer == direcoes.DIREITA) {
            this.context.drawImage(this.imagemDireita, this.currentPlayerFrameEsquerdaDireita * this.widthPlayerCharacter, 0, this.widthPlayerCharacter, this.heightPlayerCharacter, this.x, this.y, this.widthPlayerCharacter + this.personagemResize, this.heightPlayerCharacter + this.personagemResize);
        } else if (this.direcaoPlayer == direcoes.PARADO) {
            this.context.drawImage(this.imagemParado, this.currentPlayerFrameParado * this.widthPlayerCharacter, 0, this.widthPlayerCharacter, this.heightPlayerCharacter, this.x, this.y, this.widthPlayerCharacter + this.personagemResize, this.heightPlayerCharacter + this.personagemResize);
        } else if (this.direcaoPlayer == direcoes.DISPARO) {
            this.context.drawImage(this.imagemDisparo, this.currentPlayerFrameDisparo * this.widthPlayerCharacter, 0, this.widthPlayerCharacter, this.heightPlayerCharacter, this.x, this.y, this.widthPlayerCharacter + this.personagemResize, this.heightPlayerCharacter + this.personagemResize);
        }
    }
}
